//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//create student one
// let studentOneName = 'John';
// let studentOneEmail = 'john@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

//create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

// //actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects

// Use an object literal
//ENCAPSULATION

// Activity ==============

// 1. What is the term given to unorganized code that's very hard to work with?
// Ans : "Spaghetti code"
// 2. How are object literals written in JS?
// Ans : by declaring "this."
// 3. What do you call the concept of organizing information and
// functionality to belong to an object?
// Ans : 
// 4. If studentOne has a method named enroll(), how would you invoke it?
// Ans : studentOne.enroll()
// 5. True or False: Objects can have objects as properties.
// Ans : True
// 6. What is the syntax in creating key-value pairs?
// Ans : let obj = {key1: value1, key2: value2};
// 7. True or False: A method can have no parameters and still work.
// Ans : True
// 8. True or False: Arrays can have objects as elements.
// Ans : True
// 9. True or False: Arrays are objects.
// Ans : True
// 10. True or False: Objects can have arrays as properties.
// Ans : True


let studentOne = {
    name: 'John',
    email: 'john@email.com',
    grades: [89, 84, 78, 88],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        console.log(`${this.name}'s grades are: ${this.grades}`);
    },

    //Mini-Activity
   //Create a function/method that will get/compute the quarterly average of studentOne's grades.
    averageGrade() {
        // average = grades.reduce((a, b) => a + b, 0) / grades.length;
        
        let sum = 0;
        // this.grades.forEach(function(num) { sum += num });
        // return average = sum / this.grades.length;

        this.grades.forEach(grade => sum = sum + grade);
        return sum / this.grades.length;
        // for (let number of this.grades) {
        // sum += number;
        // }
        // return average = sum / this.grades.length;

    },

    // Mini-Exercise
    // Create a function/method willPass() that will return true if the average is >=85 and false if not.

    willPass() {
        // if (this.averageGrade >= 85) {
        //     return true;
        // } else {
        //     return false;
        // };

        return this.averageGrade() >= 85 ? true : false;

        // return this.averageGrade() >= 85 ? `Student ${this.name} passed.` : `Student ${this.name} failed.`;

    },
    
    // Mini-Exercise 3
        // Create a  function/method willPassWithHonors() that will return true if the student has passed and their average grade is >= 90. The function returns if either one is met.
    
    willPassWithHonors() {
        return (this.averageGrade() >= 90 && this.willPass() == true) ? true : false;
    }
}

// console.log(studentOne);
// console.log(`Student one's name is ${studentOne.name}`);
// console.log(`Student one's email is ${studentOne.email}`);
// console.log(`Student one's grades is ${studentOne.grades}`);
// console.log(studentOne.averageGrade());
// console.log(studentOne.willPass());
// console.log(studentOne.willPassWithHonors());

let studentTwo = {
    name: 'Joe',
    email: 'joe@email.com',
    grades: [78, 82, 79, 85],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        console.log(`${this.name}'s grades are: ${this.grades}`);
    },

    averageGrade() {
        let sum = 0;

        this.grades.forEach(grade => sum = sum + grade);
        return sum / this.grades.length;
    },

    willPass() {
        return this.averageGrade() >= 85 ? true : false;
    },

    willPassWithHonors() {
        return (this.averageGrade() >= 90 && this.willPass() == true) ? true : false;
    }
}

let studentThree = {
    name: 'Jane',
    email: 'jane@email.com',
    grades: [87, 89, 91, 93],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        console.log(`${this.name}'s grades are: ${this.grades}`);
    },

    averageGrade() {
        let sum = 0;

        this.grades.forEach(grade => sum = sum + grade);
        return sum / this.grades.length;
    },

    willPass() {
        return this.averageGrade() >= 85 ? true : false;
    },

    willPassWithHonors() {
        return (this.averageGrade() >= 90 && this.willPass() == true) ? true : false;
    }
}

let studentFour = {
    name: 'Jessie',
    email: 'jessie@email.com',
    grades: [91, 89, 92, 93],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        console.log(`${this.name}'s grades are: ${this.grades}`);
    },

    averageGrade() {
        let sum = 0;

        this.grades.forEach(grade => sum = sum + grade);
        return sum / this.grades.length;
    },

    willPass() {
        return this.averageGrade() >= 85 ? true : false;
    },

    willPassWithHonors() {
        return (this.averageGrade() >= 90 && this.willPass() == true) ? true : false;
    }
}

let students  = [studentOne, studentTwo, studentThree, studentFour];

// console.log(students);

let classOf1A = {

    countHonorStudents() {
        return students.filter(student => student.willPassWithHonors()).length;
    },
    honorsPercentage() {
        let answer = (this.countHonorStudents() / students.length)*100;
        return `${answer}%`;
    }
};